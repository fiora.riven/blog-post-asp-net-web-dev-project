﻿using Bloggie.Web.Models.Domains;
using Microsoft.EntityFrameworkCore;

namespace Bloggie.Web.Data
{
    public class BloggyDbContext : DbContext
    {
        public BloggyDbContext(DbContextOptions<BloggyDbContext> options) : base(options)
        {
        }


        public DbSet<BlogPost> BlogPosts { get; set; }
        public DbSet<Tag> Tags { get; set; }

        public DbSet<BlogPostLike> BlogPostLike { get; set; }

        public DbSet<BlogPostComment> BlogPostComment { get; set; }

    }
}
