﻿namespace Bloggie.Web.Models.ViewModels
{
    public class User
    {
        public Guid id { get; set; }

        public string Username { get; set; }

        public string EmailAddress { get; set; }
    }
}
