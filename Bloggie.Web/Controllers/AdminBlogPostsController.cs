﻿using Bloggie.Web.Models.Domains;
using Bloggie.Web.Models.ViewModels;
using Bloggie.Web.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Bloggie.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminBlogPostsController : Controller
    {
        private readonly ITagRepository tagRepository;
        private readonly IBlogPostRepository blogPostRepository;

        public AdminBlogPostsController(ITagRepository tagRepository,IBlogPostRepository blogPostRepository)
        {
            this.tagRepository = tagRepository;
            this.blogPostRepository = blogPostRepository;
        }

        [HttpGet]
        public async Task< IActionResult> Add()
        {
            var tags = await tagRepository.GetAllAsync();
            var model = new AddBlogPostRequest
            {
                Tags = tags.Select(x => new SelectListItem { Text = x.DisplayName, Value = x.id.ToString() })
            };  
            return View(model);

        }
        [HttpPost]
        public async Task<IActionResult> Add(AddBlogPostRequest addBlogPostRequest) {

            var blogPost = new BlogPost
            {
                Heading = addBlogPostRequest.Heading,
                Title = addBlogPostRequest.Title,
                Content = addBlogPostRequest.Content,
                ShortDescription = addBlogPostRequest.ShortDescription,
                FeaturedImageUrl = addBlogPostRequest.FeaturedImageUrl,
                UrlHandle = addBlogPostRequest.UrlHandle,
                PublicshedDate = addBlogPostRequest.PublicshedDate,
                Author = addBlogPostRequest.Author,
                Visible = addBlogPostRequest.Visible,

            };
            //map tags
            var selectedTags=new List<Tag>();
            foreach(var selectedTagId in addBlogPostRequest.SelectedTags)
            {
                var selectedTagIdAsGuid=Guid.Parse(selectedTagId);
                var existingTag = await tagRepository.GetAsync(selectedTagIdAsGuid);

                if (existingTag != null)
                {
                    selectedTags.Add(existingTag);

                }
            }

            blogPost.Tags = selectedTags;



            await blogPostRepository.AddAsync(blogPost);
            return RedirectToAction("Add");
        }

        [HttpGet]
        public async Task<IActionResult>List()
        {
            var blogPosts= await blogPostRepository.GetAllAasync();
            return View(blogPosts);
        }

        [HttpGet]
        public async Task<IActionResult>Edit(Guid id)
        {
            //retrive result from the repository
          var blogPost=  await blogPostRepository.GetAsync(id);
            var tagsDomainModel= await tagRepository.GetAllAsync();

            if (blogPost != null)
            {


                //map the domain model intoa  the view model
                var model = new EditBlogPostRequest
                {
                    Id = blogPost.Id,
                    Heading = blogPost.Heading,
                    Title = blogPost.Title,
                    Content = blogPost.Content,
                    Author = blogPost.Author,
                    FeaturedImageUrl = blogPost.FeaturedImageUrl,
                    UrlHandle = blogPost.UrlHandle,
                    ShortDescription = blogPost.ShortDescription,
                    PublicshedDate = blogPost.PublicshedDate,
                    Visible = blogPost.Visible,
                    Tags = tagsDomainModel.Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.id.ToString()
                    }),
                    SelectedTags = blogPost.Tags.Select(x => x.id.ToString()).ToArray()
                };
                return View(model); 
            }
            // pass data to view 

            return View(null);
        }

        [HttpPost]
        public async Task<IActionResult>Edit(EditBlogPostRequest editBlogPostRequest)
        {
            // map view model back to domain model
            var blogPostDomainModel = new BlogPost
            {
                Id = editBlogPostRequest.Id,
                Heading = editBlogPostRequest.Heading,
                Title = editBlogPostRequest.Title,
                Content = editBlogPostRequest.Content,
                Author = editBlogPostRequest.Author,
                ShortDescription = editBlogPostRequest.ShortDescription,
                FeaturedImageUrl = editBlogPostRequest.FeaturedImageUrl,
                UrlHandle = editBlogPostRequest.UrlHandle,
                PublicshedDate = editBlogPostRequest.PublicshedDate,
                Visible = editBlogPostRequest.Visible,

            };

            //MAP tags into domain model

            var selectedTags = new List<Tag>();
            foreach(var selectedTag in editBlogPostRequest.SelectedTags)
            {
                if(Guid.TryParse(selectedTag,out var tag))
                {
                    var foundTag =await tagRepository.GetAsync(tag);

                    if(foundTag != null)
                    {
                        selectedTags.Add(foundTag);
                    }
                }

            }

            blogPostDomainModel.Tags= selectedTags;

            //submit information to repository to update
           var updatedBlog= await blogPostRepository.UpdateAsync(blogPostDomainModel);

            if(updatedBlog != null)
            {
                //show sucssess not
                return RedirectToAction("Edit");
            }

            //redirect to get 
            return RedirectToAction("Edit");

        }
        [HttpPost]
        public async Task<IActionResult>Delete(EditBlogPostRequest editBlogPostRequest)
        {
            //talk to repository to delete this blog post and tags
           var deletedBlogPost = await blogPostRepository.DeleteAsync(editBlogPostRequest.Id);
            if (deletedBlogPost != null)
            {
                return RedirectToAction("List");
            }

            return RedirectToAction("Edit", new {id = editBlogPostRequest.Id});

            //display the response
        }

    }
}
