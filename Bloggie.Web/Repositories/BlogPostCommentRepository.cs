﻿using Bloggie.Web.Data;
using Bloggie.Web.Models.Domains;
using Microsoft.EntityFrameworkCore;

namespace Bloggie.Web.Repositories
{
    public class BlogPostCommentRepository : IBlogPostCommentRepository
    {
        private readonly BloggyDbContext bloggyDbContext;

        public BlogPostCommentRepository(BloggyDbContext bloggyDbContext)
        {
            this.bloggyDbContext = bloggyDbContext;
        }
        public async Task<BlogPostComment> AddAsync(BlogPostComment blogPostComment)
        {
            await bloggyDbContext.BlogPostComment.AddAsync(blogPostComment);
            await bloggyDbContext.SaveChangesAsync();
            return blogPostComment;
        }

        public async Task<IEnumerable<BlogPostComment>> GetCommentsByBlogIdAsync(Guid blogPostId)
        {
            return await bloggyDbContext.BlogPostComment.Where
                (x => x.BlogPostId == blogPostId).ToListAsync();
            
        }
    }
}
