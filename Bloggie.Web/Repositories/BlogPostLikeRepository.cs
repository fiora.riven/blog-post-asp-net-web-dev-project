﻿using Bloggie.Web.Data;
using Bloggie.Web.Models.Domains;
using Microsoft.EntityFrameworkCore;

namespace Bloggie.Web.Repositories
{
    public class BlogPostLikeRepository : IBlogPostLikeRepository
    {
        private readonly BloggyDbContext bloggyDbContext;

        public BlogPostLikeRepository(BloggyDbContext bloggyDbContext)
        {
            this.bloggyDbContext = bloggyDbContext;
        }

        public async Task<BlogPostLike> AddLikeForBlog(BlogPostLike blogPostLike)
        {
            await bloggyDbContext.BlogPostLike.AddAsync(blogPostLike);
            await bloggyDbContext.SaveChangesAsync();
            return blogPostLike;
        }

        public async Task<IEnumerable<BlogPostLike>> GetLikesForBlog(Guid blogPostId)
        {
           return  await bloggyDbContext.BlogPostLike.Where(x=>x.BlogPostId== blogPostId).ToListAsync();
        }

        public async Task<int> GetTotalLikes(Guid blogPostId)
        {
           return  await bloggyDbContext.BlogPostLike.
                CountAsync(x => x.BlogPostId == blogPostId);        }
    }
}
