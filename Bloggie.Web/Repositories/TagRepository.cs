﻿using Bloggie.Web.Data;
using Bloggie.Web.Models.Domains;
using Microsoft.EntityFrameworkCore;

namespace Bloggie.Web.Repositories
{


    public class TagRepository : ITagRepository
    {
        private readonly BloggyDbContext bloggyDbContext;

        public TagRepository(BloggyDbContext bloggyDbContext)
        {
            this.bloggyDbContext = bloggyDbContext;
        }
        public async Task<Tag> AddAsync(Tag tag)
        {
            await bloggyDbContext.Tags.AddAsync(tag);
            await bloggyDbContext.SaveChangesAsync();
            return tag;
        }

        public async Task<Tag?> DeleteAsync(Guid id)
        {
           var existingTag= await bloggyDbContext.Tags.FindAsync(id);
            if(existingTag != null)
            {
                bloggyDbContext.Tags.Remove(existingTag);
                await bloggyDbContext.SaveChangesAsync();
                return existingTag;
            }
            return null;

        }

        public async Task<IEnumerable<Tag>> GetAllAsync()
        {
           return await bloggyDbContext.Tags.ToListAsync();
        }

        public Task<Tag?> GetAsync(Guid id)
        {
            return bloggyDbContext.Tags.FirstOrDefaultAsync(x=>x.id==id);
        }

        public async Task<Tag?> UpdateAsync(Tag tag)
        {
            var existingTag= await bloggyDbContext.Tags.FindAsync(tag.id);
            if (existingTag != null)
            {
                existingTag.Name=tag.Name;
                existingTag.DisplayName=tag.DisplayName;
                await bloggyDbContext.SaveChangesAsync();
                return existingTag;
            }
            return null;

        }
        
    }
}
